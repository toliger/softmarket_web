---
title: Rappord du projet de Programmation WEB
author: Oliger Timothée
date: 14 décembre 2018
---

# Introduction

# Conception

J'ai choisi de faire le projet en micro services.
Cette architecture permet de monter en charge très facilement car les services sont conteneurisé et
prêt à être lancer.

# PHP

j'ai d'abord realiser le layout dans laravel. J'ai d'abord utilisé un modèle classique MVC et
d'envoyer les information dans des vue fournit par le module VueJS natif de laravel.
Dans un second temps, j'ai commencé à créer une API Restful grace à laravel et délégué tout le
front-end au routeur de VueJS.
Cependant n'étant pas un convaincu du modèle MVC et après une mise à jour de l'énnoncé, j'ai décidé
d'abandonné l'architecture et de partir sur quelquechose qui me parait plus actuel.

Cependant j'ai pu récupérer une partie du layout pour la nouvelle version en micro services.

# API

Pour ce projet, j'ai décider de créer une API graphQL.

## GraphQL

Pour ce faire, j'utilise [appolo-server[(https://www.apollographql.com/docs/apollo-server/) qui
permet de faire des API graphQL robuste.

## Knex et Booshief

Pour le stockage de données, j'utilise l'ORM [Bookshelf-js](https://www.apollographql.com/docs/apollo-server/] qui utilise le constructeur de requête [knex-js](https://knexjs.org/).
Grace à Bookshelf il suffit d'une commande pour migrer la base de donnée PostgreSQL à la version
actuelle (migrate) ou de retourner à un etat précédent (rollback).
Par manque de temps, les requetes ne sont pas très optimisés.

# Front-end

Pour ce projet, j'ai décider de partir sur VueJS.

## VueJS

VueJS est un moteur de front-end récent très complet et soutenu par une grande communauté.

## Vuex

Pour utiliser des données à travers plusieurs vues j'utilise Vuex.
Vuex permet de stoquer des informations comme l'état du panier, il sauvegarde ensuite le panier dans
le localStorage.

## Appolo-client

Appolo permet de se connecter à une API graphQL, Il suffit de créer des fichiers .gql contenant les
requetes et de leurs passer des paramètres.
Pour fonctionner, appolo envoie des requetes POST, GET, OPTIONS au serveur avec

## Vuetify && Bootstraop

Vuetify contient divers modules comme l'internationalisation, plus largement Vuetify est composé de
materialize. J'utilise vuetify notamment pour le formulaire de commande ou pour les etoiles dans la
notation des produits.

J'utilise bootstrap pour le layout générale comme la barre de navigation.

# Problèmes

Le principal contre temps à été de repartir à zéro avec quelques semaines de retard sur mon
estimation des delais.

La création du front-end à été aussi très nauséabonde, n'utilisant pas bootstrap dans mes projets
web, j'ai du lire la documentation, et cela m'a pris plus de temps que prévu.

En commençant la nouvelle architecture, j'ai d'abord conçu un maximum de choses coté client pour
soulager le serveur.
Je me suis ensuite rendu compte que le travail demander était plutot sur la partie back-end, j'ai
ainsi migrer un maximum de composants vers la partie serveur.

# Deploiement

J'utilise kubernetes pour deployer le projet. Kubernetes est un orchestrateur de docker.

# Conclusion

Ce projet m'a permis d'explorer en profondeur laravel en m'apprenant qu'on est pas fait l'un pour
l'autre.
J'ai également pu découvrir plusieurs features de bootstrap et materialize.
L'abstraction d'une base de donnée avec un ORM procure plusieurs avantages comme l'éfficacité et la
propreté du code . Malheuresement etant novice avec cet ORM, le projet n'est pas très propre.
