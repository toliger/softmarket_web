# softmarket_web

## Front

```sh
npm i && npm run serve
```

## API

```sh
npm i && docker-compose up
```

Pour générer la base de donnée

```sh
docker-compose run api npm run migrate
```
